import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LinkWorkingTest {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;


	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="http://demo.guru99.com/test/newtours/";
		driver.get(baseurl);
		
		String undercontnTitle="Under Construction: Mercury Tours";
		
		driver.manage().timeouts().implicitlyWait(5 ,TimeUnit.SECONDS);
		
		WebElement tableelement=driver.findElement(By.xpath("//div/table"));
		
		List<WebElement> alllink= tableelement.findElements(By.tagName("a"));
		
		String[] linktext= new String[alllink.size()];
		int i=0;
		
		for(WebElement e:alllink)
		{
		
			linktext[i]=e.getText();
			
			//System.out.println(linktext[i]);
			i++;
		}
		
//test links working or under construction
for(String t:linktext)
		{
			driver.findElement(By.linkText(t)).click();
			if(driver.getTitle() .equals(undercontnTitle)) {
			System.out.println("\""+t +"\"" +"under construction");
				
			}else {
			System.out.println("\"" +t+"\"" +"is working");
				
			}
			driver.navigate().back();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
				
			//driver.close();	
			
		}
		
		
	}

}

	