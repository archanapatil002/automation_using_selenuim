import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ListLinkTest {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="http://demo.guru99.com/test/newtours/";
		driver.get(baseurl);
		
		List<WebElement> alllinks= driver.findElements(By.tagName("a"));
		
		System.out.println(alllinks.size());
		
		for(WebElement link:alllinks)
		{
			System.out.println(link.getText());
			
		}
		driver.close();
		

	}

}
