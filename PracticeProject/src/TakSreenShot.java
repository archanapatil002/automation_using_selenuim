import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;

public class TakSreenShot {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;

	public static void main(String[] args) throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="https://www.google.com/";
		Thread.sleep(5000);
		
		driver.get(baseurl);
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		FileHandler.copy(src , new File("C:/Users/laptop/Desktop/archana learning-java/google.png"));

	}

}
