import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChkBoxTest {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="http://demo.guru99.com/test/radio.html";
		driver.get(baseurl);
		
		WebElement chkbox1=driver.findElement(By.id("vfb-6-0"));
		WebElement chkbox2=driver.findElement(By.id("vfb-6-1"));
		WebElement chkbox3=driver.findElement(By.id("vfb-6-2"));
		
		chkbox1.click();
		chkbox2.click();
		 
		int count=0;		
		
			if(chkbox1.isSelected())
				count=count+1;
			if(chkbox2.isSelected())
				count=count+1;
			if(chkbox3.isSelected())
				count=count+1;
		
			
		
		System.out.println("num of chk box selected is" +count);
		driver.close();

	}

}
