import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioBtnTest {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="http://demo.guru99.com/test/radio.html";
		driver.get(baseurl);
		
		WebElement radio1=driver.findElement(By.id("vfb-7-1"));
		WebElement radio2=driver.findElement(By.id("vfb-7-2"));
		WebElement radio3=driver.findElement(By.id("vfb-7-3"));
		
		radio1.click();
		if(radio1.isEnabled())
		System.out.println("radio1 enabled");
		
		radio2.click();
		if(radio2.isDisplayed())
		System.out.println("radio2 enabled");
		
		radio3.click();
		if(radio3.isDisplayed())
		System.out.println("radio3 enabled");
		
		driver.close();
		
		
		

	}

}
