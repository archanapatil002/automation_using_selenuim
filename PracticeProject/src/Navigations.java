import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Navigations {
	public static String driverpath="C:/selenium/chromedriver_win32/";
	public static WebDriver driver;


	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");
		driver= new ChromeDriver();
		
		String baseurl="http://demo.guru99.com/test/newtours/";
		driver.get(baseurl);
		driver.navigate().to("https://www.amazon.com/");
		driver.navigate().back();
		
		driver.navigate().forward();
		driver.navigate().refresh();
	}
	


}
