import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HomepageTest {
	public String baseurl="http://demo.guru99.com/test/newtours/";
	public  String driverpath="C:/selenium/chromedriver_win32/";
	public  WebDriver driver;
	
	
	@Test
	public void Homepageverify() {
		System.out.println("launch chrome browser");
		System.setProperty("webdriver.chrome.driver",driverpath+"chromedriver.exe");	
		driver=new ChromeDriver();
		driver.get(baseurl);
		String exepctedTitle="Welcome: Mercury Tours";
		String actualTitle=driver.getTitle();
		Assert.assertEquals(actualTitle, exepctedTitle);
		driver.close();
		
	}

}
