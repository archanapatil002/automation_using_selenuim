import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNGFstPrm {
	@BeforeSuite 	
	public void setUp(){
		System.out.println("set up system property for chrome");
	}
	
	@BeforeClass
	public void launchBrowser() {
		System.out.println("launch chrome browser");
		
	}
	
	@BeforeTest
	public void login() {
		System.out.println("login to app");
		
	}
	
	@BeforeMethod
	public void enterURL() {
		System.out.println("enter URL");
		
	}
	
	@Test
	public void googleTitleTest() {
		
		System.out.println("Test the Title");
		
	}
	
	@AfterMethod
	public void logout() {
		System.out.println("close url");
				
	}
	
	@AfterTest
	public void deleteallcookies() {
		System.out.println("delete cookies");
	}
	
	@AfterClass
	public void closeBrowser() {
		System.out.println("close browser");
	}
	
	@AfterSuite
	public void generateTestReport() {
		System.out.println("");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
